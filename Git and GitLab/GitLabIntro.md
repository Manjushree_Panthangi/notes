# GitLab

->What is GitLab?
GitLab is an open source code repository and collaborative software development platform. GitLab is free for individuals.
GitLab offers a location for online code storage and capabilities for issue tracking and CI/CD(Continuous Integration, Delivery, and Deployment which means you can have schedules,jobs and pipelines for your work). The repository enables hosting different development chains and versions, and allows users to inspect previous code and roll back to it in the event of problems.

->Git , GitHub and GitLab
Git : Git is a distributed version control system that lets you locally track changes and push or pull changes from remote resources.
GitHub and GitLab : Are services that provide remote access to Git repositories. In addition to hosting your code, the services provide additional features designed to help manage the software development lifecycle. These additional features include managing the sharing of code between different people, bug tracking, wiki space and other tools for 'social coding'.


- GitLab also provides a free private repository.
- GitLab provides users to see project development charts.
