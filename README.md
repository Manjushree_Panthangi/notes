# Contents

|SLNo.|Title        | Description                 |
|:--- |    :----:   |                        ---: |
|1.   | [Git and GitLab](https://gitlab.com/Manjushree_Panthangi/notes/-/tree/main/Git%20and%20GitLab)      |About Git and GitLab<br>Git Cheat Sheet<br>Markdown Cheat Sheet|
|2.   | [Agile](https://gitlab.com/Manjushree_Panthangi/notes/-/tree/main/Agile)               |Initiatives, Epics and UserStroies <br>Software Methodologies                      |
