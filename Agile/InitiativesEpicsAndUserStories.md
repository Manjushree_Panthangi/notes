# Initiatives, Epics and User Stories

To build a product you need to pick a methodology for management and discipline. Agile is one of the approaches for project management. 
Agile involves roles like Product owner,Team lead/Scrum master,Development team and Testing team etc.
When it comes to breaking down your Agile projects, the definitions become more straightforward. You will come across certain terms such as Initiatives, epics, stories, and tasks.

**Initiative :** An initiative is the end goal or an objective.<br>
**Example :** To develop and launch a school management system. 

**Epic :** Epic is a large body of work that needs to be done to make progress to the end goal /Or Large Modules of work.<br>
**Example :** Login System, Attendance and Finance.
 
**User Stories :** Small detailed and specific work to be done to complete an epic.<br>
**Example :** Creating Student login page, Creating Parent Login page etc.
